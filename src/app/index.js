'use strict';

angular.module('gulpa', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainCtrl'
            })
            .state('resp', {
                url: 'resp',
                templateUrl: 'app/resp/resp.html',
                controller: 'RespCtrl'
            })
            .state('boot', {
                url: '/boot',
                templateUrl: 'app/boot/boot.html',
                controller: 'BootCtrl'
            })
            .state('strap', {
                url: '/strap',
                templateUrl: 'app/strap/strap.html',
                controller: 'StrapCtrl'
            })
            .state('video', {
                url: '/video',
                templateUrl: 'app/video/video.html',
                controller: 'VideoCtrl'
            });
        $urlRouterProvider.otherwise('/');
  })
;
